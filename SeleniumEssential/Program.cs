﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

var driver = new ChromeDriver();

try
{
    driver.Url = "https://www.epam.com";

    var searchIcon = driver.FindElement(By.CssSelector(".search-icon"));

    searchIcon.Click();

    WebDriverWait searchWait = new(driver, TimeSpan.FromSeconds(5))
    {
        PollingInterval = TimeSpan.FromMilliseconds(250),
        Message = "Search header has not been displayed"
    };

    searchWait.Until(driver => driver.FindElement(By.Name("q")).Displayed);

    driver.FindElement(By.Name("q")).SendKeys("Automation");
}
catch { }
finally
{
    driver?.Quit();
}